# File IO Demo

An introduction to data persistence using StreamWriters and StreamReaders to read from and write to local .txt and .csv files. For the Sweden delivery August 2023.

©️ Noroff Accelerate

## Installation

Clone the repository by running the command:
```bash
git clone https://gitlab.com/teacher.warren.english/file-io-demo-sweden-23.git
```

## Contributing

@teacher.warren.english

## License

[MIT](https://choosealicense.com/licenses/mit/)
