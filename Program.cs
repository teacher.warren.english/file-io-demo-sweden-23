﻿using CsvHelper;
using System.Globalization;

const string CSV_FILE_PATH = @"cars.csv";

#region Text File Stuff
// write to a txt file
static void WriteToTextFile()
{
    using StreamWriter sw = new(@"cars.txt", true);
    sw.WriteLine("Hello world again! 🌍");
    sw.WriteLine("Hello world again again! 🌍");
    sw.WriteLine("Hello world again again again! 🌍");

}

//WriteToTextFile();

// read from a txt file
static void ReadFromTextFile()
{
    try
    {
        using StreamReader sr = new(@"cars.txt");

        string line = string.Empty;

        while ((line = sr.ReadLine()) != null)
        {
            Console.WriteLine(line);
        }
    }
    catch (FileNotFoundException ex)
    {
        Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
}

//ReadFromTextFile();

#endregion

// write to a csv file
static void WriteToCsvFile()
{
    using StreamWriter sw = new(CSV_FILE_PATH);
    using CsvWriter writer = new(sw, CultureInfo.InvariantCulture);

    // get list of cars
    // write the list of cars to the csv file
    writer.WriteRecords(GetCarsOld());
}

WriteToCsvFile();

static List<Car> GetCars()
{
    List<Car> cars = new List<Car>
    {
        new Car() { Brand = "Ford", Model = "Mustang", Price = 19999.99 },
        new Car() { Brand = "Chevrolet", Model = "Corvette", Price = 39799.99 },
        new Car() { Brand = "DMC", Model = "De Lorean", Price = 19999.99 },
        new Car() { Brand = "Chevrolet", Model = "Camaro", Price = 19999.99 },
        new Car() { Brand = "Volvo", Model = "XC90", Price = 19999.99 },
    };

    return cars;
}

static List<Car> GetCarsOld()
{
    List<Car> cars = new()
    {
        new Car("Nissan", "350Z", 99.99),
        new Car("Aston Martin", "DB9", 92.95),
        new Car("Jaguar", "X-Type", 87.66),
    };

    return cars;
}

// read from a csv file
static void ReadFromCsvFile()
{
    try
    {
        using StreamReader sr = new(CSV_FILE_PATH);
        using CsvReader reader = new(sr, CultureInfo.InvariantCulture);

        IEnumerable<Car> records = reader.GetRecords<Car>();
        
        Console.WriteLine("READING CAR DATA:");

        foreach (var car in records)
        {
            Console.WriteLine($"{car.Brand} {car.Model} {car.Price} ");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
}

ReadFromCsvFile();